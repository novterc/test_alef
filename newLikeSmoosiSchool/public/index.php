<?php

use src\AppKernel;
use Symfony\Component\HttpFoundation\Request;

require_once __DIR__ . '/../bootstrap.php';

$config = include __DIR__ . '/../config.php';
$kernel = AppKernel::getInstance();
$kernel->setDevMode(true);
$kernel->setConfig($config);

$request = Request::createFromGlobals();
$response = $kernel->handlerHtppRequest($request);
$response->send();
