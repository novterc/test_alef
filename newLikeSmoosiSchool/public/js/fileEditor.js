
var urlAjaxFileUpload = '/?method=handlerimage';


function createSelectFileDialog($selectCallback) {
    var input = $(document.createElement('input'));
    input.attr("type", "file");
    $(input).on('change', function (e) {

        $selectCallback(e.target.files);
    });
    input.trigger('click');
}

function getUploadFile() {
    createSelectFileDialog(function (files) {
        uploadFile(files);
    });
}

function uploadFile(files) {
    var formdata = new FormData();

    for (var key in files) {
        var file = files[key];
        formdata.append("file" + key, file);
    }

    var ajax = new XMLHttpRequest();
    ajax.addEventListener("load", completeHandler, false);
    ajax.addEventListener("error", errorHandler, false);
    ajax.addEventListener("abort", abortHandler, false);
    ajax.open("POST", urlAjaxFileUpload);
    ajax.send(formdata);
}

function errorHandler(event) {
    swal("Error!", "Upload Failed!", "error");
}

function abortHandler(event) {
    swal("Error!", "Upload Aborted!", "error");
}

function completeHandler(event) {
    if (event.target.status == 200) {
        try {
            var jsonData = JSON.parse(event.target.responseText);
        } catch (e) {
            swal("Error!", 'invalid response json', "error");
            return false;
        }
        if (jsonData.status != undefined) {
            if (jsonData.status == 'success') {
                if (jsonData.data != undefined && jsonData.data.count != undefined) {
                    swal("Success!", 'Found ' + jsonData.data.count + ' pixels', "success");
                } else {
                    console.log('Error: invalid respons');
                }
            } else if(jsonData.status == 'fail') {
                swal("Error!", jsonData.reason, "error");
            } else {
                console.log('Error: invalid respons');
            }
        }
    } else {
        swal("Error!", event.target.statusText, "error");
    }
}
