<?php

namespace src\Controller;

use src\Util\PixelFinder;
use src\Util\PixelIterator;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use src\Util\ImageResourceFactory;

class MainController extends Controller
{
    /**
     * Index action
     * @return Response
     */
    public function indexAction()
    {
        return $this->render('layouts/main.html.twig');
    }

    /**
     * Count pixel by
     * @param Request $request
     */
    public function countPixelAction(Request $request)
    {
        $findColorIndex = 16777215;
        $file = $request->files->get('file0');
        if ($file) {
            $imageResurce = ImageResourceFactory::factory($file);
            if ($imageResurce) {
                $iterator = new PixelIterator($imageResurce);
                $pixelFinder = new PixelFinder($iterator);
                $foundPixel = $pixelFinder->findByColorIndex($findColorIndex);

                return new JsonResponse([
                    'status' => 'success',
                    'data' => [
                        'count' => $foundPixel,
                    ],
                ]);
            }
        }

        return new JsonResponse(['status' => 'fail']);
    }

}
