<?php

namespace src\Controller;

use src\AppKernel;
use Symfony\Component\HttpFoundation\Response;
use Twig_Loader_Filesystem;
use Twig_Environment;

abstract class Controller
{
    /**
     * Renders a view.
     *
     * @param string   $view       The view name
     * @param array    $parameters An array of parameters to pass to the view
     * @param Response $response   A response instance
     *
     * @return Response A Response instance
     */
    protected function render($view, array $parameters = array(), Response $response = null)
    {
        if (null === $response) {
            $response = new Response();
        }

        $response->setContent($this->getTwig()->render($view, $parameters));

        return $response;
    }

    protected function getTwig()
    {
        $app = AppKernel::getInstance();
        $loader = new Twig_Loader_Filesystem($app->getConfig('paths.templates'));
        $twig = new Twig_Environment($loader);

        return $twig;
    }

}
