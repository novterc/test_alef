<?php

namespace src\Util;

use Symfony\Component\HttpFoundation\File\UploadedFile;

class ImageResourceFactory
{
    /**
     * ImageResourceFactory constructor.
     * @param UploadedFile $file
     * @return mixed
     */
    public static function factory(UploadedFile $file)
    {
        switch ($file->getMimeType()) {
            case 'image/jpeg':
                return imagecreatefromjpeg($file->getPathname());
            case 'image/png':
                return imagecreatefrompng($file->getPathname());
            default:
                return false;
        }
    }

}
