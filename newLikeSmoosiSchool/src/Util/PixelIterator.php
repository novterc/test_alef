<?php

namespace src\Util;

class PixelIterator implements \Iterator
{
    protected $resource;
    protected $x = 0;
    protected $y = 0;
    protected $maxX = 0;
    protected $maxY = 0;

    public function __construct($imageResource)
    {
        $this->resource = $imageResource;
        $this->maxX = (imagesx($imageResource) -1);
        $this->maxY = (imagesy($imageResource) -1);
    }

    public function current()
    {
        return imagecolorat($this->resource, $this->x, $this->y);
    }

    public function next()
    {
        if($this->x >= $this->maxX) {
            $this->y++;
        } else {
            $this->x++;
        }
    }

    public function key()
    {
        return $this->y * $this->x + $this->x;
    }

    public function valid()
    {
        return ($this->key() <= $this->maxY * $this->maxX);
    }

    public function rewind()
    {
        $this->x = 0;
        $this->y = 0;
    }

}
