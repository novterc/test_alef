<?php

namespace src\Util;

class PixelFinder
{
    protected $iterator;

    public function __construct(\Iterator $iterator)
    {
        $this->iterator = $iterator;
    }

    public function findByColorIndex($colorIndex)
    {
        $totalPixel = 0;
        foreach ($this->iterator as $value) {
            if ($value === $colorIndex) {
                $totalPixel++;
            }
        }

        return $totalPixel;
    }

}
