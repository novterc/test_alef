<?php

namespace src;

use src\Controller\MainController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class AppKernel
{
    protected static $instance;
    protected $config;
    protected $isDevMode;


    /**
     * Get instance
     * @return AppKernel
     */
    public static function getInstance()
    {
        if (self::$instance === null) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    public function setDevMode(bool $status)
    {
        $this->isDevMode = $status;
    }

    /**
     * Set config
     * @param array
     */
    public function setConfig(array $config)
    {
        $this->config = $config;
    }

    /**
     * Get config data by path
     * @param string $path
     * @return array|mixed
     * @throws \Exception
     */
    public function getConfig($path = '')
    {
        $keyArr = explode('.', $path);
        $config = $this->config;
        if (!empty($keyArr)) {
            foreach ($keyArr as $key) {
                if (!isset($key)) {
                    throw new \Exception('not found config path:' . $path);
                }
                $config = $config[$key];
            }
        }

        return $config;
    }

    /**
     * Get mode status
     * @return bool
     */
    public function isDevMode()
    {
        return $this->isDevMode;
    }

    /**
     * Handle http request instance
     * @param Request $request
     * @return Response
     */
    public function handlerHtppRequest(Request $request)
    {
        try {
            $callable = $this->getControllerAndMethodByRequest($request);
            $response = call_user_func($callable, $request);
        } catch (\Exception $exception) {
            $response = new Response();
            $response->setStatusCode(500);
            if ($this->isDevMode()) {
                $response->setContent($exception->getMessage() . '-' . $exception->getFile() . ':' . $exception->getLine());
            }
        }

        return $response;
    }

    /**
     * Get controller and method by request
     * @param Request $request
     * @return array
     */
    public function getControllerAndMethodByRequest(Request $request)
    {
        switch ($request->get('method', null)) {
            case 'handlerimage':

                return [new MainController(), 'countPixelAction'];
            default:

                return [new MainController(), 'indexAction'];
        }
    }

}
