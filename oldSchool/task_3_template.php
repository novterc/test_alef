<!doctype html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport"
              content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Document</title>
    </head>
    <body>
        <h1>White pixel counter</h1>
        <form action="" method="POSt" enctype="multipart/form-data">
            <input type="file" accept="image/x-png,image/jpeg" name="image">
            <br>
            <input type="submit" value="load">
        </form>
        <?php if($showResult): ?>
        <section>
            <h2>Result for <?=$fileName?></h2>
            <p><?=$countPixel?></p>
        </section>
        <?php endif; ?>
    </body>
</html>