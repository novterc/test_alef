<?php

$valueLimit = 10000;
$sumTotal = 0;
$beforeLast = 0;
$last = 1;

while (true) {
    $sumItem = $beforeLast + $last;
    if ($sumItem > $valueLimit) {
        break;
    }
    $sumTotal += $sumItem;
    $beforeLast = $last;
    $last = $sumItem;
}

var_dump($sumTotal);
