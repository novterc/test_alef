<?php

if ($_FILES['image']) {
    $imageResource = getImageResourceByFile($_FILES['image']);
    if($imageResource !== false ) {
        $fileName = $_FILES['image']['name'];
        $findColorIndex = 16777215;
        $countPixel = pixelCounterByImageResourceAndColorIndex($imageResource, $findColorIndex);
        $showResult = true;
    }
}

require 'task_3_template.php';


function pixelCounterByImageResourceAndColorIndex($resource, $colorIndex)
{
    $countPixel = 0;
    $width = imagesx($resource);
    $height = imagesy($resource);

    for ($x = 0; $x < $width; $x++) {
        for ($y = 0; $y < $height; $y++) {
            $color = imagecolorat($resource, $x, $y);
            if ($color === $colorIndex) {
                $countPixel++;
            }
        }
    }

    return $countPixel;
}

function getImageResourceByFile($imageFile)
{
    if ($imageFile['type'] === 'image/jpeg') {
        return imagecreatefromjpeg($imageFile['tmp_name']);
    } elseif ($imageFile['type'] === 'image/png') {
        return imagecreatefrompng($imageFile['tmp_name']);
    }

    return false;
}
