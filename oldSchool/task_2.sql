SELECT *, DATEDIFF(users.birthday, NOW()) as day_before_birthday
FROM users
HAVING day_before_birthday < 30 ;